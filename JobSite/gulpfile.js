/**
 * Created by Zuhaib Kathwari on 01-08-2016.
 */
var gulp = require("gulp");

var concat = require('gulp-concat');

gulp.task('scripts', function() {
    gulp.src([
        './bower_components/jquery/dist/jquery.min.js',
        './bower_components/bootstrap/dist/js/bootstrap.min.js',
        './bower_components/angular/angular.min.js',
        './bower_components/angular-route/angular-route.min.js',
        './node_modules/socket.io-client/socket.io.js',
        './node_modules/socketio-file-upload/client.js',
        './node_modules/ngstorage/ngStorage.min.js',
        './bower_components/angular-auto-validate/dist/jcs-auto-validate.js',
        './www/lib/nav-plugin.js',
        './www/lib/shieldui-all.min.js'
    ])
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./www/lib'))
});

gulp.task('devjs', function() {
    gulp.src([
        './www/js/app.js',
        './www/js/router.js',
        './www/js/service.js',
        './www/js/directive.js',
        './www/js/uploadAPI.js',
        './www/js/loginController.js',
        './www/js/home.js',
        './www/js/regCtrl.js',
        './www/js/consaltancy.js',
        './www/js/StudentProfileView.js',
        './www/js/StudentProfileUpdate.js',
        './www/js/dashboard.js',
        './www/js/widgets/feed.js',
        './www/js/widgets/jobs.js'
    ])
    .pipe(concat('dev-scripts.js'))
    .pipe(gulp.dest('./www/js'))
});

gulp.task('watch', function() {
    gulp.watch('./www/js/*.js',['devjs']);
    gulp.watch('./www/lib/*.js',['scripts']);
});

gulp.task('default' , ['scripts', 'devjs']);