/**
 * This file contain wrapper functions for database related operations
 */
var mongodb = require("./mongodb").mongodb ;
var mongoConn = new mongodb();
var dbOpearation = function(){
	console.log("In dbOperation : Constructor");
	return this ;
};

dbOpearation.prototype = {
		constructor : dbOpearation
};

dbOpearation.prototype.getData = function(tableName , key , iGetDataCB){
	console.log("In  dbOperation : getData");
	try{
		mongoConn.getData(tableName, key, function(iError , iResponse){
			if(iGetDataCB){
				return iGetDataCB(iError , iResponse);
			}
		});
	}catch(err){
		console.log("Exception in dbOperation : getData : Exception : " , err);
	}
}

dbOpearation.prototype.insertData = function(TableName , Key , Data ,iInsertDataCB){
	try{
		mongoConn.insertData(TableName , Key , Data, function(iError , iResponse){
			if(iInsertDataCB){
				return iInsertDataCB(iError , iResponse);
			}
		});
	}catch(err){
		console.log("Exception in dbOperation : insertData : Exception : " , err);
	}
}

dbOpearation.prototype.updateData = function(TableName , Key , Data , iUpdateDataCB){
	try{
		mongoConn.updateData(TableName, Key, Data, function(iError , iResponse){
			if(iUpdateDataCB){
				return iUpdateDataCB(iError , iResponse);
			}
		})
	}catch(err){
		console.log("Exception in dbOperation : updateData : Exception : " , err);
	}
}

dbOpearation.prototype.deleteData = function(TableName , Key , iDeleteDataCB){
	try{
		
	}catch(err){
		console.log("Exception in dbOperation : deleteData : Exception : " , err);
	}
}

exports.dbOpearation = dbOpearation;
/*
var fun = new dbOpearation();
var dd = function(){
	console.log("DDDDFF");
	console.log("WWWWWWWW");
}
var ff = [];
ff[0] = {geo: [1,2],
		request : {f : "l"},
		routes : [{gh : dd}]}
fun.insertData("TestTable", "myKey", {pk : "myKey" , value : ff}, function(){
	console.log("SUCCESS");
})*/
//fun.getData("TestTable", {pk : "myKey"}, function(iError , iData){
//	console.log("RRRRRRRTT" , iError);
//	console.log("XXXXXXXXX" , iData);
//	var ff = iData[1].value;
//	console.log("FF : "  ,ff);
//});