/**
 * This file contain wrapper functions for database related operations
 */
var mongodb = require("./mongodb").mongodb ;
var mongoConn = new mongodb()
var dbOpearation = function(){
	console.log("In dbOperation : Constructor");
	return this ;
};

dbOpearation.prototype = {
		constructor : dbOpearation
};

dbOpearation.prototype.getData = function(tableName , key , iGetDataCB){
	console.log("In  dbOperation : getData");
	try{
		mongoConn.getData(tableName, key, function(iError , iResponse){
			
			if(iGetDataCB){
				return iGetDataCB(iError , iResponse);
			}
		});
	}catch(err){
		console.log("Exception in dbOperation : getData : Exception : " , err);
	}
}

dbOpearation.prototype.getAllData = function(tableName , iGetDataCB){
	console.log("In  dbOperation : getAllData");
	try{
		mongoConn.getAllData(tableName,function(iError , iResponse){
			if(iGetDataCB){
				return iGetDataCB(iError , iResponse);
			}
		});
	}catch(err){
		console.log("Exception in dbOperation : getData : Exception : " , err);
	}
}

dbOpearation.prototype.insertData = function(TableName , Data ,iInsertDataCB){
	console.log("In dbOperation : insertData");
	
	try{
		mongoConn.insertData(TableName ,Data, function(iError , iResponse){
			console.log("In dbOperation : insertData",iError , iResponse);
			if(iInsertDataCB){
				return iInsertDataCB(iError , iResponse);
			}
		});
	}catch(err){
		console.log("Exception in dbOperation : insertData : Exception : " , err);
	}
}

dbOpearation.prototype.insertMany = function(TableName , DataArray ,iInsertManyCB){
	try{
		mongoConn.insertMany(TableName ,Data, function(iError , iResponse){
			if(iInsertManyCB){
				return iInsertManyCB(iError , iResponse);
			}
		});
	}catch(err){
		console.log("Exception in dbOperation : insertData : Exception : " , err);
	}
}

dbOpearation.prototype.updateData = function(TableName , Key , Data , iUpdateDataCB){
	try{
		mongoConn.updateData(TableName, Key, Data, function(iError , iResponse){
			if(iUpdateDataCB){
				return iUpdateDataCB(iError , iResponse);
			}
		})
	}catch(err){
		console.log("Exception in dbOperation : updateData : Exception : " , err);
	}
}

dbOpearation.prototype.deleteData = function(TableName , Key , iDeleteDataCB){
	try{
		
	}catch(err){
		console.log("Exception in dbOperation : deleteData : Exception : " , err);
	}
}

dbOpearation.prototype.getAllUserGroup = function(iGetAllUserGroupCB){
	console.log("In  dbOperation : getAllUserGroup");
	try{
		mongoConn.getAllUserGroup("user_group",function(iError , iResponse){
			if(iGetAllUserGroupCB){
				return iGetAllUserGroupCB(iError , iResponse);
			}
		});
	}catch(err){
		console.log("Exception in dbOperation : getAllUserGroup : Exception : " , err);
	}
}

exports.dbOpearation = dbOpearation;
