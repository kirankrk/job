/**
 * @author SBN
 * 
 * This will have mongodb operations 
 */
 
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var mongodburl = 'mongodb://localhost:27017/vyaprut';
//console.log("MongoClient" , MongoClient);

/*MongoClient.connect(mongodburl, function(err, db) {
	  assert.equal(null, err);
	  console.log("Connected correctly to server");

	  db.close();
});*/

var mongodb = function(){
	console.log("In mongodb : constructor");
	this.a = null;
	MongoClient.connect(mongodburl, function(err, db) {
		  assert.equal(null, err);
		  console.log("Connected correctly to server");
		  this.dbConn = db ;
		  return this ;
	})
	
};

mongodb.prototype ={
		constructor:mongodb
};

mongodb.prototype.initConn = function(initConnCB){
	MongoClient.connect(mongodburl, function(err, db) {
		  assert.equal(null, err);
		  console.log("Connected correctly to server");
		  this.dbConn = db ;
		  if(initConnCB){
			return initConnCB(db)  
		  }
		 // return this ;
	})
	
};
mongodb.prototype.getData = function(tableName , searchKey , iGetDataCB){
	var _that = this ;
	
	try{
		this.initConn(function(db){
			db.collection(tableName).find(searchKey).limit(2).toArray(function(err, docs) {
			      console.log("Get Data callback");
			      console.log(err, docs);
			      if(iGetDataCB){
			    	  return iGetDataCB(err, docs);
			      }
			});
		});
		
		
	}catch(err){
		console.log("Exception in mongodb : getData : Exception : " , err);
	}
}
/**
 * Function for inserting data in mongodb
 * 
 */
mongodb.prototype.insertData = function(TableName , Key , Data ,iInsertDataCB){
	var _that = this ;
	try{
		this.initConn(function(db){
			db.collection(TableName).insertOne(Data, function(err, r) {
				console.log("insert Call Back");
				console.log(err, r);
				if(iInsertDataCB){
					return iInsertDataCB(err, r);
				}
			});
		})
		
	}catch(err){
		console.log("Exception in mongodb : insertData : Exception : " , err);
	}
}

mongodb.prototype.updateData = function(TableName , Key , Data , iUpdateDataCB){
	var _that = this ;
	try{
		this.initConn(function(db){
			db.collection(TableName).updateOne(Key, {$set: Data}, function(err, r) {
				console.log("updateOne callback");
				console.log(err, r);
				if(iUpdateDataCB){
					return iUpdateDataCB(err, r);
				}
			});
		})
		
	}catch(err){
		console.log("Exception in mongodb : updateData : Exception : " , err);
	}
}

mongodb.prototype.deleteData = function(TableName , Key , iDeleteDataCB){
	try{
		
	}catch(err){
		console.log("Exception in mongodb : deleteData : Exception : " , err);
	}
}

exports.mongodb = mongodb;

/*var fun = new mongodb();
var obj = {
		fname : "Sachin",
		lname : "Nagesh",
		username : "sachin.vyaprut@gmail.com",
		password : "sv"
};
for(var i = 0 ; i < 10 ; i++){
	fun.insertData("test_table", {pk : i},{_id : i ,myVal : i ,pkstr : i + "ert" })

}*/
/*fun.getData("user_info",{ username : "sachin.ghgvyaprut@gmail.com"}, function(iError , iData){
	console.log("SSSSSSSSSLLLLLLL{PPPPPPPPP");
	console.log(iError , iData);
});*/