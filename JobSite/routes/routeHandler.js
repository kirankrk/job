/**
 * This is route handler file
 * 
 */
var dbOperation = require("./dbOperation").dbOpearation;
var dbConn = new dbOperation();
//console.log("SSSSSSSS");
//dbConn.getData();
var Handler = function(){
	console.log("In routeHandler : constructor");
	return this;

}
Handler.prototype.checkLogin = function(iRequest , iReply){
	var _that = this ;
	console.log("In routeHandler  : checkLogin");
	console.log(iRequest.payload);
	if (iRequest.auth.isAuthenticated) {
        return iReply.redirect('/');
    }
	if(iRequest.payload.username){
		dbConn.getData("user_info" , {email : iRequest.payload.username},function(iError,iDataArr){
			console.log("Check 1");
			console.log(iError,iDataArr);
			if(iError){
				iReply({user : iRequest.payload.username , status : "FAIL",message : "Password is incorrect !!!"});
			}else{
				if(iDataArr && iDataArr[0]){
					var _userinfo = iDataArr[0];
					if(iRequest.payload.password == _userinfo.password){
						var _responceObject = {
							userInfo : 	_userinfo,
							userId : _userinfo['email'],
							status : "SUCCESS",
							message : "LOGIN SUCCESS",
							SUCCESS : true,
							userType : iDataArr[0]['userType'],
							data : {
								userInfo : 	_userinfo
							}
						};
						console.log("_responceObject : " , _responceObject);
						iRequest.auth.session.set({id : new Date().getTime() , username : iRequest.payload.username});
						iReply(_responceObject);
					}else{
						iReply({user : iRequest.payload.username , SUCCESS : false,status : "FAIL",message : "Password is incorrect !!!"});
					}
				}else{
					iReply({user : iRequest.payload.username , SUCCESS : false,status : "FAIL",message : "User Does Not Exist"});
				}
			}
			
			
		});
		
	}else{//.payload.username
		iReply({user : iRequest.payload.username , status : "FAIL", message : "User Does Not Exist"});
	}
};

Handler.prototype.registerUser = function(iRequest , iReply){
	console.log("In routeHandler  : registerUser");
	var _that = this ;
	var _userInfo = {};
//	_userInfo['firstName'] = iRequest.payload.firstName ; 
//	_userInfo['lastName'] = iRequest.payload.lastName;
	console.log("reg iRequest.payload",iRequest.payload);
	console.log(typeof iRequest.payload);
	_userInfo['username'] = iRequest.payload.email;
  	_userInfo['password'] = iRequest.payload.password;
  	var _userId = _userInfo['username'];
  	
  	dbConn.getData("user_info" , {_id : _userInfo['username'],email : _userId},function(iError,iDataArr){
  		if(iError){
  			iReply({user : _userId, status : "FAIL",message : iError});
  		}else{
  			if(iDataArr && iDataArr[0]){
  				iReply({user : _userId, status : "FAIL",message : "User Already Exist !!!"});
  			}else{
  				_userInfo = iRequest.payload ;
  				_userInfo['_id'] = iRequest.payload.email;
  				dbConn.insertData("user_info",_userInfo,function(iError , iResponce){
  			  		if(iError){
  			  			iReply({user : _userId, status : "FAIL",message : iError});
  			  		}else{
  			  			iReply({user : _userInfo ,SUCCESS : true,message : "User Registered Successfully !!!"});
  			  		}
  			  	});
  			}
  		}
  	});
  	
  	
};

Handler.prototype.getConsultancyFilter = function(request, reply){
	var _filter = request.payload;
		console.log("in dbConn getConsultancyFilter request ",request.payload);
	_filter['userType'] = 'consultancy';
	dbConn.getData('user_info',_filter,function(iError , iResponce){
		console.log("in dbConn getConsultancyFilter");
		if(iError){
			reply({status : "Error at server side"});
		}else{
			reply({status : "SUCCESS",data : iResponce});
		}
	});
}


Handler.prototype.getJobPostFilter = function(request, reply){
	var _filter = request.payload;
	dbConn.getData('job_post',_filter,function(iError , iResponce){
		if(iError){
			reply({status : "Error at server side"});
		}else{
			reply({status : "SUCCESS",data : iResponce});
		}
	});
}

Handler.prototype.getStudentResumeFilter = function(request, reply){
	var _filter = request.payload;
	dbConn.getData('student_resume',_filter,function(iError , iResponce){
		if(iError){
			reply({status : "Error at server side"});
		}else{
			reply({status : "SUCCESS",data : iResponce});
		}
	});
}

Handler.prototype.getStudentFilter = function(request, reply){
	var _filter = request.payload;
	_filter['userType'] = 'student';
	dbConn.getData('user_info',_filter,function(iError , iResponce){
		if(iError){
			reply({status : "Error at server side"});
		}else{
			reply({status : "SUCCESS",data : iResponce});
		}
	});
}

Handler.prototype.getAllStates = function(request , reply){
	reply(["Andaman and Nicobar Islands","Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chandigarh","Chhattisgarh","Dadra and Nagar Haveli","Daman and Diu","Delhi","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala","Lakshadweep","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Orissa","Pondicherry","Punjab","Rajasthan","Sikkim","Tamil Nadu","Tripura","Uttaranchal","Uttar Pradesh","West Bengal"]);
}

Handler.prototype.getCitiesByState = function(request , reply){
	var _stateName = request.payload.state ;
	dbConn.getData('state_city_map',{state : _stateName},function(iError , iResponce){
		if(iError){
			reply({status : "Error at server side"});
		}else{
			reply({status : "SUCCESS",data : iResponce});
		}
	});
}

Handler.prototype.logout = function (request, reply) {

    request.auth.session.clear();
    return reply.redirect('/');
}

exports.Handler = Handler ;/**
 * This is route handler file
 * 
 */
var dbOperation = require("./dbOperation").dbOpearation;
var dbConn = new dbOperation();
//console.log("SSSSSSSS");
//dbConn.getData();
var Handler = function(){
	console.log("In routeHandler : constructor");
	return this;

}
Handler.prototype.checkLogin = function(iRequest , iReply){
	var _that = this ;
	console.log("In routeHandler  : checkLogin");
	console.log(iRequest.payload);
	if(iRequest.payload.username){
		dbConn.getData("user_info" , {email : iRequest.payload.username},function(iError,iDataArr){
			console.log("Check 1");
			console.log(iError,iDataArr);
			if(iError){
				iReply({user : iRequest.payload.username , status : "FAIL",message : "Password is incorrect !!!"});
			}else{
				if(iDataArr && iDataArr[0]){
					var _userinfo = iDataArr[0];
					if(iRequest.payload.password == _userinfo.password){
						var _responceObject = {
							userInfo : 	_userinfo,
							userId : _userinfo['email'],
							status : "SUCCESS",
							message : "LOGIN SUCCESS",
							SUCCESS : true,
							userType : iDataArr[0]['userType'],
							data : {
								userInfo : 	_userinfo
							}
						};
						console.log("_responceObject : " , _responceObject);
						iReply(_responceObject);
					}else{
						iReply({user : iRequest.payload.username , SUCCESS : false,status : "FAIL",message : "Password is incorrect !!!"});
					}
				}else{
					iReply({user : iRequest.payload.username , SUCCESS : false,status : "FAIL",message : "User Does Not Exist"});
				}
			}
			
			
		});
		
	}else{//.payload.username
		iReply({user : iRequest.payload.username , status : "FAIL", message : "User Does Not Exist"});
	}
};

Handler.prototype.registerUser = function(iRequest , iReply){
	console.log("In routeHandler  : registerUser");
	var _that = this ;
	var _userInfo = {};
//	_userInfo['firstName'] = iRequest.payload.firstName ; 
//	_userInfo['lastName'] = iRequest.payload.lastName;
	console.log("reg iRequest.payload",iRequest.payload);
	console.log(typeof iRequest.payload);
	_userInfo['username'] = iRequest.payload.email;
  	_userInfo['password'] = iRequest.payload.password;
  	var _userId = _userInfo['username'];
  	
  	dbConn.getData("user_info" , {_id : _userInfo['username'],email : _userId},function(iError,iDataArr){
  		if(iError){
  			iReply({user : _userId, status : "FAIL",message : iError});
  		}else{
  			if(iDataArr && iDataArr[0]){
  				iReply({user : _userId,SUCCESS : false, status : "FAIL",message : "User Already Exist !!!"});
  			}else{
  				_userInfo = iRequest.payload ;
  				_userInfo['_id'] = iRequest.payload.email;
  				dbConn.insertData("user_info",_userInfo,function(iError , iResponce){
  			  		if(iError){
  			  			iReply({user : _userId,SUCCESS : false, status : "FAIL",message : iError});
  			  		}else{
  			  			iReply({user : _userInfo ,SUCCESS : true,message : "User Registered Successfully !!!"});
  			  		}
  			  	});
  			}
  		}
  	});
  	
  	
};

Handler.prototype.getConsultancyFilter = function(request, reply){
	var _filter = request.payload;
		console.log("in dbConn getConsultancyFilter request ",request.payload);
	_filter['userType'] = 'consultancy';
	dbConn.getData('user_info',_filter,function(iError , iResponce){
		console.log("in dbConn getConsultancyFilter");
		if(iError){
			reply({status : "Error at server side"});
		}else{
			reply({status : "SUCCESS",data : iResponce});
		}
	});
}


Handler.prototype.getJobPostFilter = function(request, reply){
	var _filter = request.payload;
	dbConn.getData('job_post',_filter,function(iError , iResponce){
		if(iError){
			reply({status : "Error at server side"});
		}else{
			reply({status : "SUCCESS",data : iResponce});
		}
	});
}

Handler.prototype.getStudentResumeFilter = function(request, reply){
	var _filter = request.payload;
	dbConn.getData('student_resume',_filter,function(iError , iResponce){
		if(iError){
			reply({status : "Error at server side"});
		}else{
			reply({status : "SUCCESS",data : iResponce});
		}
	});
}

Handler.prototype.getStudentFilter = function(request, reply){
	var _filter = request.payload;
	_filter['userType'] = 'student';
	dbConn.getData('user_info',_filter,function(iError , iResponce){
		if(iError){
			reply({status : "Error at server side"});
		}else{
			reply({status : "SUCCESS",data : iResponce});
		}
	});
}

Handler.prototype.getAllStates = function(request , reply){
	reply(["Andaman and Nicobar Islands","Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chandigarh","Chhattisgarh","Dadra and Nagar Haveli","Daman and Diu","Delhi","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala","Lakshadweep","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Orissa","Pondicherry","Punjab","Rajasthan","Sikkim","Tamil Nadu","Tripura","Uttaranchal","Uttar Pradesh","West Bengal"]);
}

Handler.prototype.getCitiesByState = function(request , reply){
	var _stateName = request.payload.state ;
	dbConn.getData('state_city_map',{state : _stateName},function(iError , iResponce){
		if(iError){
			reply({status : "Error at server side"});
		}else{
			reply({status : "SUCCESS",data : iResponce});
		}
	});
}

Handler.prototype.postJob = function(request , reply){
	var jobPost = request.payload;
	dbConn.insertData("job_post",jobPost , function(iError, iResponce) {
	    reply(iError, iResponce);
	})
}

Handler.prototype.logout = function (request, reply) {

    request.auth.session.clear();
    return reply.redirect('/');
}

exports.Handler = Handler ;