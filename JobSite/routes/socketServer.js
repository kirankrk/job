/*global angular*/
/**
 *@author Sachin N
 *@purpose This is socket server file
 * 
 */

var dbOperation = require("./dbOperation").dbOpearation;
var dbConn = new dbOperation();
var processData = require("./vProcessData").processData;
var proData = new processData();
var routesHandler = require('./routeHandler').Handler;
var Handler = new routesHandler();
var emailOperations = require("./EmailOperations").emailOperations;
var EmailOperation = new emailOperations();
console.log("proData : ", proData);

var serverSocket = function() {
	console.log("In socketServer : Constructor");
	return this;
};

serverSocket.prototype.init = function(iSocket) {
	var socket = iSocket;
	socket.on("checkLogin", function(iData, iCheckLoginCB) {
		var _userInfo = iData;
		var _userPassword = iData.passsword;
		var _userName = iData['username'];
		
		console.log("uaernmae  : ", _userName);
		console.log("iData from socket server : ", iData);
		Handler.checkLogin({payload: {username:_userName}}, iCheckLoginCB);
	});
	
	
	socket.on("registerUser", function(iData, iRegisterUserCB) {
		var _userInfo = iData;
		var _userId = _userInfo['username'];
		console.log("sockt server in reg user");
		dbConn.getData("user_info" , {_id : _userInfo['username'],email : _userId}, function(iError, iDataArr) {
			if (iError) {
				if (iRegisterUserCB) {
					return iRegisterUserCB({
						userInfo: iData,
						status: "FAIL", 
						message: iError
					});
				}
			}
			else {
				if (iDataArr && iDataArr[0]) {
					if (iRegisterUserCB) {
						return iRegisterUserCB({
							userInfo: iData,
							status: "FAIL",
							message: "User Already Exist !!!"
						});
					}
				}
				else {
					dbConn.insertData("user_info",_userInfo, function(iError, iResponce) {
						if (iError) {
							if (iRegisterUserCB) {
								return iRegisterUserCB({
									userInfo: iData,
									status: "FAIL",
									message: iError
								});
							}
						}
						else {
							if (iRegisterUserCB) {
								return iRegisterUserCB({
									userInfo: iData,
									status: "SUCCESS",
									message: "User Registered Successfully !!!"
								});
							}
						}
					});
				}
			}
		});
	});


	socket.on("getStates", function(reqInfo, callback) {
		console.log("In socketServer : getStates", reqInfo);

		Handler.getAllStates({
			payload: reqInfo
		}, callback);
	});


	socket.on("getCities", function(reqInfo, callback) {
		console.log("In socketServer : getCities");
		Handler.getCitiesByState({
			payload: reqInfo
		}, callback)
	});
	
	
	socket.on("sendMail", function(reqInfo, callback) {
		console.log("In socketServer : sendMail");
		EmailOperation.sendMail(reqInfo, callback)
	});
	
/*	socket.on("getAreas", function(reqInfo, callback) {
		console.log("In socketServer : getAreas");
		Handler.getAreasByCity({
			payload: reqInfo
		}, callback)
	});*/


	socket.on("getAreas", function(reqInfo, callback) {
		console.log("In socketServer : getAreas");
		callback(['Shanivar Peth', 'Swargate', 'Hadapsar', 'Karve Nager']);
	});

	socket.on("getConsultancyList", function(reqInfo, callback) {
		console.log("In socketServer : getConsultancyList", reqInfo);
		Handler.getConsultancyFilter({
			payload: reqInfo
		}, callback);
	});

	socket.on("getConsultancyData", function(reqInfo, callback) {
		console.log("In socketServer : getConsultancyData");
		Handler.getConsultancyFilter({
			payload: reqInfo
		}, callback);
	});
	socket.on("setConsultancyData", function(reqInfo, callback) {
		console.log("In socketServer : setConsultancyData");
		callback(['Job Consultancy 1', 'PUne ', 'Karve Nagar', '500']);
	});

	socket.on("getConsultancyFilter", function(reqInfo, callback) {
		console.log("In socketServer : getConsultancyFilter");
		Handler.getConsultancyFilter({
			payload: reqInfo
		}, callback)
	});

	socket.on("getStudentFilter", function(reqInfo, callback) {
		console.log("In socketServer : getStudentFilter");
		Handler.getConsultancyFilter({
			payload: reqInfo
		}, callback)
	});

	socket.on("getJobPostFilter", function(reqInfo, callback) {
		console.log("In socketServer : getJobPostFilter");
		Handler.getJobPostFilter({
			payload: reqInfo
		}, callback)
	});

	socket.on("postJob", function(jobInfo, callback) {
		console.log("In socketServer : getJobPostFilter");
		Handler.postJob({
			payload: jobInfo
		}, callback)
	});
	
	socket.on('updateConsultancy',function(iData , iUpdateConsultancyCB){
			console.log("In socketServer : updateConsultancy data");
			var _userInfo = iData;//['userInfo'];
			//var _userId = _userInfo['userInfo'];
			console.log("In socketServer : updateConsultancy _userInfo data kiran",_userInfo);
			//_userInfo['selectedCity'] = _userInfo['selectedCity']["city"];
			
					dbConn.updateData("user_info",{_id : _userInfo['_id']},_userInfo, function(iError, iResponce) {
						console.log("In callback")
						//console.log(iError, iResponce)
						if (iError) {
							if (iUpdateConsultancyCB) {
								return iUpdateConsultancyCB({
									userInfo: iData,
									status: "FAIL",
									message: iError
								});
							}
						}
						else {
							if (iUpdateConsultancyCB) {
								return iUpdateConsultancyCB({
									userInfo: iData,
									status: "SUCCESS",
									message: "Information Updated Successfully !!!"
								});
							}
						}
					});
				
			
	});
	
	

};

exports.serverSocket = serverSocket;
