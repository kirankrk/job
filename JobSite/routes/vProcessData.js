/**
 *@author : Sachin N 
 */
var dbOperation = require("./dbOperation").dbOpearation;
var dbConn = new dbOperation();

var processData = function(){
	console.log("In processData : constructor");
	return this;
};

processData.prototype ={
		constructor:processData
};

processData.prototype.insertProcessData = function(UserInfo , Action , DataArray , InsertProcessDataCB){
	console.log("In processData : insertProcessData");
	var _userId = UserInfo.userId ;
	for(var i = 0 ; i < DataArray.length ; i++){
		var _data = DataArray[i];
		var _key = _data['KEY'] ; 
		var _tableName = _data['TN'];
		dbConn.insertData(_tableName,_userId,_data,function(iError , iResponce){
			console.log("In processData : insertProcessData : dbConn.insertData :CallBAck");
			console.log(iError , iResponce);
			if(InsertProcessDataCB){
				return InsertProcessDataCB(iError , iResponce);
			}
		});
	}
	
};

processData.prototype.getData = function(tableName , searchKey , iGetDataCB){
	dbConn.getData(tableName , searchKey , iGetDataCB);
}

exports.processData = processData;