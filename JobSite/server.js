var Hapi = require('hapi');
var socketIOFileUpload = require('socketio-file-upload');
var nodemailer = require('nodemailer');
var routesHandler = require('./routes/routeHandler').Handler;
console.log("routesHandler : ", routesHandler);
var Handler = new routesHandler();

// Create a server with a host and port
var server = new Hapi.Server();

server.connection({
    host: process.env.IP,
    port: process.env.PORT
});

/*var Inert = require('inert');

server.register(Inert);
*/
server.register(require('hapi-auth-cookie'), function() {
    server.auth.strategy('session', 'cookie', {
        password: 'itsasecretone',
        cookie: 'a-sid-example',
        redirectTo: '/',
        isSecure: false
    });
    // setROutes('session',{'hapi-auth-cookie' : {redirectTo : false}});
});


server.views({
    engines: {
        html: require('handlebars'),
        jade: require('jade')
    },
    path: __dirname + '/www'
});


var _routesArr = [{
        method: 'GET',
        path: '/',
        handler: function(request, reply) {
            return reply.file('./www/index.html');
        }
    }, {
        method: 'GET',
        path: '/{path*}',
        handler: {
            directory: {
                path: './www/',
                listing: false,
                index: true
            }
        }
    }, {
        method: 'POST',
        path: '/checkLogin',
        handler: function(request, reply) {
            console.log("In server : checkLogin");
            console.log(request.payload);
            Handler.checkLogin(request, reply);
            /*if(request.payload && request.payload.username == "sachin.nagesh@gmail.com" && request.payload.password == "sachin"){
        	
        	var _responseObj = {
        			userId : request.payload.username,
        			userType : "consultancy",
        			name : 'Sachin',
        			charges : 100,
        			address : "Karve Nager , Pune"
        	};
        	
        	reply({userInfo : {userId : request.payload.username },SUCCESS : true , userType : "consultancy", data : [_responseObj]});
    	
        }else if(request.payload && request.payload.username == "kiran.kothawade@gmail.com" && request.payload.password == "kiran"){
        	var _responseObj = {
        			userId : request.payload.username,
        			userType : "consultancy",
        			name : 'Kiran'
        	};
        	reply({userInfo : {userId : request.payload.username },SUCCESS : true ,userType : "student", data : [_responseObj]});
        }else{
        	reply({userInfo : {userId : request.payload.username},SUCCESS : false });
        }*/

            //Handler.checkLogin(request , reply)
            //return reply.file('./www/index.html');
        }
    }, {
        method: 'POST',
        path: '/registerUser',
        handler: function(request, reply) {
            console.log('registerUser');
            Handler.registerUser(request, reply)
        }
    }, {
        method: 'POST',
        path: '/getStates',
        handler: function(request, reply) {
            /*console.log('POST /getStates request: ', request);*/
            Handler.getAllStates(request, reply)

        }
    }, {
        method: 'POST',
        path: '/getCities',
        handler: function(request, reply) {
            console.log('getCities');
            Handler.getCitiesByState(request, reply)
        }
    }, {
        method: 'POST',
        path: '/getAreas',
        handler: function(request, reply) {
            console.log('getAreas');
            console.log(request.payload);
            Handler.getAreasByCity(request, reply)
        }
    }, {
        method: 'POST',
        path: '/getConsultancyList',
        handler: function(request, reply) {
            console.log('getConsultancyList');
            console.log(request.payload);
            //reply(['Job Consultancy 1','Job Consultancy 2','Job Consultancy 3','Job Consultancy 4']);
            Handler.getConsultancyFilter(request, reply)
        }
    }, {
        method: 'POST',
        path: '/getConsultancyData',
        handler: function(request, reply) {
            console.log('getConsultancyData');
            console.log(request.payload);
            Handler.getConsultancyFilter(request, reply)
        }
    }, {
        method: 'POST',
        path: '/setConsultancyData',
        handler: function(request, reply) {
            console.log('setConsultancyData');
            console.log(request.payload);
            Handler.registerUser(request, reply)
        }
    }, {
        method: 'POST',
        path: '/getConsultancyFilter',
        handler: function(request, reply) {
            Handler.getConsultancyFilter(request, reply)
        }
    }, {
        method: 'POST',
        path: '/getStudentFilter',
        handler: function(request, reply) {
            Handler.getStudentFilter(request, reply)
        }
    }, {
        method: 'POST',
        path: '/getJobPostFilter',
        handler: function(request, reply) {
            Handler.getJobPostFilter(request, reply)
        }
    }
    /*,{
            method: 'GET',
            path: '/userImage',
            handler: function (request, reply) {
                reply.file('/resumes/to/save/uploads/head-659652_960_720-4.png');
            }
        }*/
];
server.route(_routesArr);


var io = require('socket.io')(server.listener);
var sockServer = require('./routes/socketServer').serverSocket;
var socketServer = new sockServer();

var validateResume = /([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf)$/;

server.start(function() {
    console.log('Server running at:', server.info.uri);
    io.on('connection', function(socket) {

        var uploader = new socketIOFileUpload();

        // save handler:
        uploader.on("saved", function(event) {
            socket.emit('fileUploadSuccess', event.file);
        });

        /*uploader.on("start", function(event) {
            if (/^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpeg|.JPEG|.gif|.GIF|.png|.PNG)$/.test(event.file.name)) {
                return true;
            } else {
                console.log('start event', event);

                return false;

            }
        });*/

        uploader.dir = "www/resumes/to/save/uploads/";

        uploader.listen(socket);

        socketServer.init(socket);
        console.log("Connected to socket!!!!!!");
    });
});
