/*global jobGeeks*/
jobGeeks.controller('StudProfileUpdateCntlr', ['$scope', '$location', 'areaService', 'socket',  'upload', '$localStorage',
	function($scope, $location, areaService, socket, upload, $localStorage) {
		
		$scope.userDetails = areaService.getUserLoginData(); //get user details

		upload.listen("upload_input"); //add event listener to the file input

    	socket.on('fileUploadSuccess', function (data) {//check file upload success status
        	$scope.completed =  data.success;
        	console.log('success: ', data.pathName);
        	
        	$scope.selectedImage = data.pathName.replace(/.*www/g,""); //trim absolute path to relative path
        	$scope.updateStudProfile();
    	});
    	
		socket.emit('getStates', null, function(data) { //prepopulate the user data and set the selected model
			console.log('$scope.userDetails ', $scope.userDetails);
			$scope.states = data;
			$scope.selectedState = $scope.userDetails.selectedState;
			socket.emit('getCities', {
				state: $scope.selectedState
			}, function(data) {
				$scope.cities = data.data;
				$scope.selectedCity = $scope.userDetails.selectedCity;
				console.log('$scope.selectedCity ', $scope.selectedCity);
				socket.emit('getAreas', {
					city: $scope.selectedCity
				}, function(data) {
					$scope.area = data;
					$scope.selectedArea = $scope.userDetails.selectedArea;
					console.log('$scope.selectedArea: ', $scope.selectedArea);
				});
			});
		});

		$scope.updateStudProfile = function() {
			$scope.userDetails.selectedCity = $scope.selectedCity;
			$scope.userDetails.selectedArea = $scope.selectedArea;
			$scope.userDetails.selectedState = $scope.selectedState;
			$scope.userDetails.selectedImage = $scope.selectedImage;
			console.log('#scope.userDetails', $scope.userDetails)
			socket.emit('updateConsultancy', $scope.userDetails, function(data) {
				console.log("socket updateStudProfile data: ", data);
				$location.path('/studentProfile');
			});
		};
	}
]);
