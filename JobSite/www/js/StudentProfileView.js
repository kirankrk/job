/*global jobGeeks*/
jobGeeks.controller('StudProfileViewCntlr', ['$scope', '$location', 'areaService', 'socket',  'upload', '$localStorage',
	function($scope, $location, areaService, socket, upload, $localStorage) {
		
		$scope.userDetails = areaService.getUserLoginData(); //get user details

		/*upload.listen("upload_input");*/ //add event listener to the file input
		
		upload.uploadOnClick("upload_button");//add event listener to the file input

    	socket.on('fileUploadSuccess', function (data) {//check file upload success status
        	$scope.completed =  data.success;
        	console.log('success: ', data.pathName);
        	
        	$scope.selectedImage = data.pathName.replace(/.*www/g,""); //trim absolute path to relative path
        	$scope.updateStudProfile();
    	});

		$scope.studEdit = function() {
			$location.path('/studEdit');
		};

		$scope.updateStudProfile = function() {
			/*$scope.userDetails.selectedCity = $scope.selectedCity;
			$scope.userDetails.selectedArea = $scope.selectedArea;
			$scope.userDetails.selectedState = $scope.selectedState;*/
			$scope.userDetails.selectedImage = $scope.selectedImage;
			console.log('#scope.userDetails', $scope.userDetails)
			socket.emit('updateConsultancy', $scope.userDetails, function(data) {
				console.log("socket updateStudProfile data: ", data);
				/*$location.path('/studentProfile');*/
			});
		};
	}
]);
