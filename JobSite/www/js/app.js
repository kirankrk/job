/*global angular*/
/**
 * Created by Zuhaib Kathwari on 01-07-2016.
 */
var jobGeeks = angular.module('jobApp', ['ngRoute' ,'ngStorage', 'jcs-autoValidate']);

jobGeeks.run(['$rootScope', '$location', '$route', 'areaService', '$localStorage', 'defaultErrorMessageResolver', 
function($rootScope, $location, $route, areaService, $localStorage, defaultErrorMessageResolver) {
     defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages){
            errorMessages['badEmail'] = "Email isn't valid, please enter correct email";
            errorMessages['badPassword'] = "Password should be in UpperCase, LowerCase, Number/SpecialChar and min 8 Chars";
            errorMessages['badContact'] = "Please enter a valid contact";
     });
    $rootScope.$on('$locationChangeStart', function(event, next, current) {
        // checks if next page requires login then
        // checks if user is not login
        // and redirect user to the next route
        console.log('Jobgeeeks bootstrapped');
        var routeObject = ($route.routes[$location.path()]);
        if (!areaService._isLogin && routeObject.requireLogin) {
            if ($localStorage.userDetails === undefined) {
                $location.path('/login');
            }
        }
    });
}]);