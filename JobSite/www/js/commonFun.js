jobGeeks.service('areaService', ['$http', '$rootScope', function($http, $rootScope) {
/*
	this.consultancyData = {};*/ //need to use private setter and getter methods for this later on 



	this.getData = function(states, addressData) {
		return $http({
			url: addressData,
			method: 'POST',
			data: {
				state: states
			}
		})
	};




	this.getConsultancyDataItems = function(selected, addressData) {
		return $http({
			url: addressData,
			method: 'POST',
			data: {
				email: selected
			}
		})
	};


	this.setConsultancyData = function(consultancyData) {
		this.consultancyData = consultancyData;
		console.log('this.consultancyData: ', this.consultancyData);
	}

	this.getConsultancyData = function() {
		console.log('this.getConsultancyData: ', this.consultancyData);
		return this.consultancyData;
	}



}]);