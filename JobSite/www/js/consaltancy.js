/*global jobGeeks*/
jobGeeks.controller('consultancyCtrl', ['$scope', '$http', '$location', 'areaService', 'socket',
	function($scope, $http, $location, areaService, socket) {
		
	$scope.validateEmail = areaService.validateEmail; //email validation
	$scope.validatePassword = areaService.validatePassword;
	$scope.validateContact= areaService.validateContact;
	
		/*$scope.consultancyData = areaService.getConsultancyData();*/
		if(areaService.getConsultancyData()==undefined){
			$scope.userDetails = areaService.getUserLoginData();	
		}
		else{
			$scope.userDetails = areaService.getConsultancyData();	
		}

		//$scope.userDetails = areaService.userDetails;
		$scope.validateEmail = areaService.validateEmail; //email validation
		$scope.validatePassword = areaService.validatePassword;
		console.log('areaService.userDetails: ', areaService.userDetails)
		console.log("$scope.userDetails ", $scope.userDetails); //used areaService instead of rootscope

		/*socket.emit('getStates', null, function(data) {
			console.log('states from socket.io in socketServer : ', data);
			$scope.states = data;
			$scope.selectedState = $scope.userDetails.selectedState;
			
			socket.emit('getCities', {
				state: $scope.selectedState
			}, function(data) {
				$scope.cities = data.data;
				$scope.selectedCity = $scope.userDetails.selectedCity.city;
				console.log("$scope.userDetails.selectedCity.city: ", $scope.userDetails.selectedCity.city);
				
				socket.emit('getAreas', {
					city: $scope.selectedCity
				}, function(data) {
					$scope.area = data;
					$scope.selectedArea = $scope.userDetails.selectedArea;
					console.log("socket getArea data: ", $scope.area);
				});
			});
		});*/

		socket.emit('getStates', null, function(data) {
			console.log('$scope.userDetails ', $scope.userDetails);
			$scope.states = data;
			$scope.selectedState = $scope.userDetails.selectedState;
			socket.emit('getCities', {
				state: $scope.selectedState
			}, function(data) {
				$scope.cities = data.data;
				$scope.selectedCity = $scope.userDetails.selectedCity;
				console.log('$scope.selectedCity ', $scope.selectedCity);
				socket.emit('getAreas', {
					city: $scope.selectedCity
				}, function(data) {
					$scope.area = data;
					$scope.selectedArea = $scope.userDetails.selectedArea;
					console.log('$scope.selectedArea: ', $scope.selectedArea);
				});
			});
		});

		$scope.getCity = function() {
			socket.emit('getCities', {
				state: $scope.selectedState
			}, function(data) {
				$scope.cities = data.data;
				console.log("socket getCities data: ", $scope.cities);
			});
		}


		$scope.getArea = function() {
			socket.emit('getAreas', {}, function(data) {
				$scope.area = data;
				console.log("socket getArea data: ", $scope.area);
			});
		}

		$scope.consultancyEdit = function() {
			$location.path('/consultancyEdit');
		};

		$scope.updateConsultancy = function(valid) {
			if (valid) {
				$scope.userDetails.selectedCity = $scope.selectedCity;
				$scope.userDetails.selectedArea = $scope.selectedArea;
				$scope.userDetails.selectedState = $scope.selectedState;
				socket.emit('updateConsultancy', $scope.userDetails, function(data) {
					$scope.consultancyData = data;
					$location.path('/consultancy');
				});
			} else {
				console.info('Some details are missing');
			}
		};
		
		$scope.sendJobMail = function(){
			$scope.userDetails = areaService.getUserLoginData();
			socket.emit('sendMail', {data:$scope.userDetails}, function(data) {
				console.log("Consultancy Send Mail ", data);
			});
		}
	}
]);
