/*global angular*/
/**
 * Created by Zuhaib Kathwari on 01-07-2016.
 */
var jobGeeks = angular.module('jobApp', ['ngRoute' ,'ngStorage', 'jcs-autoValidate']);

jobGeeks.run(['$rootScope', '$location', '$route', 'areaService', '$localStorage', 'defaultErrorMessageResolver', 
function($rootScope, $location, $route, areaService, $localStorage, defaultErrorMessageResolver) {
     defaultErrorMessageResolver.getErrorMessages().then(function(errorMessages){
            errorMessages['badEmail'] = "Email isn't valid, please enter correct email";
            errorMessages['badPassword'] = "Password should be in UpperCase, LowerCase, Number/SpecialChar and min 8 Chars";
            errorMessages['badContact'] = "Please enter a valid contact";
     });
    $rootScope.$on('$locationChangeStart', function(event, next, current) {
        // checks if next page requires login then
        // checks if user is not login
        // and redirect user to the next route
        console.log('Jobgeeeks bootstrapped');
        var routeObject = ($route.routes[$location.path()]);
        if (!areaService._isLogin && routeObject.requireLogin) {
            if ($localStorage.userDetails === undefined) {
                $location.path('/login');
            }
        }
    });
}]);
/*global jobGeeks*/
jobGeeks.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'templates/Login.html',
		controller : 'loginCtrl',
		requireLogin: false
	}).when('/login', {
		templateUrl : 'templates/Login.html',
		controller : 'loginCtrl',
		requireLogin: false
	}).when('/home', {
		templateUrl : 'templates/Home.html',
		controller : 'FilterCtrl',
		requireLogin: true
	}).when('/register', {
		templateUrl : 'templates/Register.html',
		controller : 'regCtrl',
		requireLogin: false
	}).when('/studentProfile', {
		templateUrl : 'templates/StudentProfileView.html',
		controller : 'StudProfileViewCntlr',
		requireLogin: true
	}).when('/consultancy', {
		templateUrl : 'templates/ConsultancyDetails.html',
		controller : 'consultancyCtrl',
		requireLogin: true
	}).when('/consultancyEdit', {
		templateUrl : 'templates/ConsultancyEdit.html',
		controller : 'consultancyCtrl',
		requireLogin: true
	}).when('/studEdit', {
		templateUrl : 'templates/StudProfileUpdate.html',
		controller : 'StudProfileUpdateCntlr',
		requireLogin: true
	}).when('/dashboard', {
		templateUrl: 'templates/dashboard.html',
		controller: 'DashboardCtrl',
		requireLogin: true
	}).otherwise({
		redirectTo : '/login'
	});
} 
]);



/*global jobGeeks, io*/
/**
 * Created by Zuhaib Kathwari on 20-06-2016.
 */
jobGeeks.service('areaService', ['$http', '$localStorage', '$rootScope', '$route', '$log', '$location', function($http, $localStorage, $rootScope, $route, $log, $location) {

	this._dataArray = [];
	this._isLogin = false;
	this.validateEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	this.validatePassword = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/i;
	this.validateContact = /^(\+\d{1,3}[- ]?)?\d{10}$/;
		
		this.userName= function(){//this subroutine checks who is logged in
			if(this.userDetails != undefined) {
				if( this.userDetails.userInfo.userType != 'student'){
					this.name = this.userDetails.userInfo.name;
				}
				else{
				 this.name = this.userDetails.userInfo.fName + " " + this.userDetails.userInfo.lName;
				}
					return {
						name: this.name,//incase of consultancy use this conditionally
						userType : this.userDetails.userInfo.userType,
						isLogin: true
					};
			} else {
				throw "User not logged in!"
			}
		}
	
	

	this.getData = function(states, addressData) {
		return $http({
			url: addressData,
			method: 'POST',
			data: {
				state: states
			}
		});
	};

	this.getConsultancyDataItems = function(selected, addressData) {
		return $http({
			url: addressData,
			method: 'POST',
			data: {
				email: selected
			}
		});
	};

	this.setConsultancyData = function(consultancyData) {
		this.consultancyData = consultancyData;
	};

	this.getConsultancyData = function() {
		return this.consultancyData;
	};


	this.setUserDetails = function(userDetails) {
		this.userDetails = userDetails;
	};

	this.getUserDetails = function() {
		return this.userDetails;
	};
	
	this.checkLogin = function(data, url) {
		return $http({
			url: url,
			method: 'POST',
			data: data
		});
	};

	this.loginDataStore = function() { //local storage
		$localStorage.userDetails = {
			userDetails: this.userDetails,
			'isLoggedIn' : true
		};
		console.info('Data Stored in local storage', $localStorage.userDetails);
	};
	
	this.getUserLoginData = function (){
		if(this.userDetails === undefined && $localStorage.userDetails.userDetails !== undefined) {
			this.userDetails = $localStorage.userDetails.userDetails;
			console.info('$localStorage.userDetails: ', $localStorage.userDetails.userDetails)
			return this.userDetails.userInfo;
		} else {
			console.info('$localStorage.userDetails: ', $localStorage.userDetails.userDetails)
			return this.userDetails.userInfo;
		}
	}

	this.isLogin = function(userLoginData) {
		console.info('user about to login: ', userLoginData);
		if (userLoginData.SUCCESS) {
			this.userDetails = userLoginData.data;
			this.loginDataStore();
			this._isLogin = true;
			/*this.userDetails = userLoginData.data;*/
			if (userLoginData.userType == 'student') {
				console.warn("Student Logged in");
				//this._dataArray = userLoginData.data;
				$location.path('/home');
			}
			else if (userLoginData.userType == 'consultancy') {
				//this.setConsultancyData(userLoginData.data.userInfo);
				console.log('areaService.consultancyData: ', this.consultancyData);
				$location.path('/consultancy');
			}
			else {
				this._isLogin = false;
			}
		}
		else {
			console.warn("WRONG_USERNAME_PASSWORD");
		}
	};
}]);




//Socket.io API
jobGeeks.factory('socket', function($rootScope) {
	var socket = io.connect();
	return {
		socketInstance: socket,
		on: function(eventName, callback) {
			socket.on(eventName, function() {
				var args = arguments;
				$rootScope.$apply(function() {
					callback.apply(socket, args);
				});
			});
		},
		emit: function(eventName, data, callback) {
			socket.emit(eventName, data, function() {
				var args = arguments;
				$rootScope.$apply(function() {
					if (callback) {
						callback.apply(socket, args);
					}
				});
			});
		}
	};
});
/*global jobGeeks*/
jobGeeks.directive('errorBox', function() {
  return {
    restrict: 'E',
    transclude : false,
    templateUrl: './templates/error-component.html',
    scope: {
      errorMessage: '@',
      errorOption: '=',
      ngIf: '@',
      ngClass: '@'
    }
  };
});
/*global  jobGeeks, SocketIOFileUpload*/
/**
 * Created by Zuhaib Kathwari on 12/7/2016.
 */
jobGeeks.factory('upload', ['socket', '$rootScope', function(socket, $rootScope) {
    console.log('upload factory loaded yes');
    // Initialize instances:
    var socket = socket.socketInstance;
    
    var siofu = new SocketIOFileUpload(socket);

    return {
        listen: function (input) {
            siofu.listenOnInput(document.getElementById(input));
        },
        uploadOnClick: function (input) {
            //takes element id as input e.g button id
            document.getElementById(input).addEventListener("click", siofu.prompt, false);
        }
    }
}]);
/*global jobGeeks*/
jobGeeks.controller('loginCtrl',['$scope', 'areaService', '$localStorage', '$location', 'socket' ,
function($scope, areaService, $localStorage, $location, socket) {
	$scope.user = {};
	$scope.validateEmail = areaService.validateEmail; //email validation
	$scope.user = areaService;
	$scope.obj = {value: 'error'};//css
	$scope.errorMessage;//show errormessage
	$scope.showError = false; //show error
	$scope.login = function() {
		var url = "/checkLogin";
		
		areaService.checkLogin($scope.user, url).success(function(response) {
		//	var _dataArray = areaService._dataArray;
			console.log('response: ', response);
			if(response.SUCCESS){
				areaService.setUserDetails(response);
					areaService.isLogin(response);	
			}
			else{
				$scope.showError = true;
				$scope.errorMessage =response.message;//show errormessage
			}
		});
	};


	$scope.logOut = function() {
		delete $localStorage.userDetails;
		areaService.userDetails = undefined;
		$location.path('/login');
		/*areaService.loginData = {logOutIcon: false, homeIcon: false, registerIcon: false,  loginIcon: true};
		$scope.loginData = areaService.loginData;*/
		console.info('Logged out');
	};
}]);

/*global jobGeeks*/
jobGeeks.controller('FilterCtrl', ['$scope', '$http', '$location', '$rootScope', 'areaService', 'socket', function($scope, $http, $location, $rootScope, areaService, socket) {
	$rootScope.urlData = 'https://job-kirankrk-2.c9users.io';
	$scope.userDetails = areaService.getUserLoginData();
	console.log('$scope.userDetails home : ', $scope.userDetails);
	/*	var empty = '', getStates = "/getStates";*/

	var reqObj = null;
	socket.emit('getStates', reqObj, function(data) {
		console.log('states from socket.io in socketServer : ', data);
		$scope.states = data;
	});

	/*var states = areaService.getData("", "/getStates");
	 */

	/*	states.success(function(response) {
			$scope.states = response;
			console.info("$scope.states from FilterCtrl: ", $scope.states)
		});*/

	/*$scope.getCity = function() {
		areaService.getData($scope.selectedState, "/getCities").success(function(response) {
			$scope.cities = response.data;
			console.log("$scope.cities $scope.selectedState in regController: ", $scope.cities, $scope.selectedState)
		});[]
	}*/
	$scope.getCity = function() {
		socket.emit('getCities', {
			state: $scope.selectedState
		}, function(data) {
			$scope.cities = data.data;
			console.log("socket getCities data: ", $scope.cities);
		});
	}


	$scope.getArea = function() {
			socket.emit('getAreas', {
				city: $scope.selectedCity.city
			}, function(data) {
				$scope.area = data.data;
				console.log("socket getArea data: ", $scope.area);
			});
		}
		/*$scope.getArea = function() {
			areaService.getData($scope.selectedCity, "/getAreas").success(function(response) {
				$scope.area = response;
				console.log("$scope.cities in regController: ", $scope.cities)
			});
		}*/

	$scope.getConsultancyList = function() {
		console.log("Get getConsultancyList: ", {
			selectedState: $scope.selectedState,
			selectedCity: $scope.selectedCity,
			selectedArea: $scope.selectedArea
		});

		/*var city = Json.stringfy($scope.selectedCity, function( key, value ) {
    			if( key === "$$hashKey" ) {
    			 return undefined;
    			}
    		return value;
		});*/

		//	
		//console.log('json city list', city);

		var obj = {
			selectedState: $scope.selectedState,
			selectedCity: $scope.selectedCity,
			selectedArea: $scope.selectedArea
		};
		delete obj.selectedCity.$$hashKey;
		console.log("obj " + obj);
		socket.emit('getConsultancyList', obj, function(data) {
			$scope.consultancies = data.data;
			console.info('socket consultancy list', data);
		});
	};

	$scope.getStudentProfile = function() {
		console.info('my profile');
		$location.path('/studentProfile');
	}

	$scope.getConsultancyData = function(selectedConsultancy) {
		socket.emit('getConsultancyData', {
			email: selectedConsultancy
		}, function(response) {
			console.log('socket consultancy list', response);
			$scope.userDetails.userInfo=response.data[0];
			$scope.userDetails.userInfo.userType = 'student';
			areaService.setConsultancyData(response.data[0]);
			$location.path('/consultancy');
		});

	};
}]);

/*global jobGeeks, SocketIOFileUpload*/
jobGeeks.controller('regCtrl', function($scope, $http, $location, $rootScope, areaService, socket) {

    $scope.user = {}; //user data model
    $scope.showError;
    $scope.errorMessage;
	var states = areaService.getData("", "/getStates");
	
	$scope.showForm = {
		name: 'student'
	}

	
	$scope.validateEmail = areaService.validateEmail; //email validation
	$scope.validatePassword = areaService.validatePassword;
	$scope.validateContact= areaService.validateContact;

	$scope.isShown = function(whichForm) {
		$scope.showForm.name = whichForm;
		console.log('$scope.showForm.name: ', $scope.showForm.name);
		/*return whichForm === $scope.user.userType;*/
	};

	states.success(function(response) {
		$scope.states = response;
		console.log("$scope.states: ", $scope.states)
	});

	$scope.getCity = function() {
		$scope.cities = '';
		areaService.getData($scope.user.selectedState, "/getCities").success(function(response) {
			$scope.cities = response.data;
			console.log("$scope.cities: ", $scope.cities)
		});
	}

	$scope.getArea = function() {
		areaService.getData($scope.user.selectedCity, "/getAreas").success(function(response) {
			$scope.area = response;
			console.log("$scope.area: ", $scope.area)
		});
	}

	$scope.register = function() {
		console.log('areaService: ', areaService)
		console.info('$scope.user: ', $scope.user);
		console.info("$scope.showForm.name ", $scope.showForm.name);
		console.info("$scope.user.email ", $scope.user.email);

		$http({
				url : '/registerUser',
				method : 'POST',
				data : $scope.user
			}).success(function(response) {
			
				if(response.SUCCESS){
					areaService.setUserDetails(response);
					console.info("response.SUCCESS ", response.message);
					console.info("response.SUCCESS in if", response.message);
					$location.path('/login');
				}
				else{
					$scope.showError = true;
					$scope.errorMessage="Please check the filled information "+ response.message;
					console.info("response.SUCCESS in else", response.message);
				     $location.path('/register');	
				}
				
			});

	};
});
/*global jobGeeks*/
jobGeeks.controller('consultancyCtrl', ['$scope', '$http', '$location', 'areaService', 'socket',
	function($scope, $http, $location, areaService, socket) {
		
	$scope.validateEmail = areaService.validateEmail; //email validation
	$scope.validatePassword = areaService.validatePassword;
	$scope.validateContact= areaService.validateContact;
	
		/*$scope.consultancyData = areaService.getConsultancyData();*/
		if(areaService.getConsultancyData()==undefined){
			$scope.userDetails = areaService.getUserLoginData();	
		}
		else{
			$scope.userDetails = areaService.getConsultancyData();	
		}

		//$scope.userDetails = areaService.userDetails;
		$scope.validateEmail = areaService.validateEmail; //email validation
		$scope.validatePassword = areaService.validatePassword;
		console.log('areaService.userDetails: ', areaService.userDetails)
		console.log("$scope.userDetails ", $scope.userDetails); //used areaService instead of rootscope

		/*socket.emit('getStates', null, function(data) {
			console.log('states from socket.io in socketServer : ', data);
			$scope.states = data;
			$scope.selectedState = $scope.userDetails.selectedState;
			
			socket.emit('getCities', {
				state: $scope.selectedState
			}, function(data) {
				$scope.cities = data.data;
				$scope.selectedCity = $scope.userDetails.selectedCity.city;
				console.log("$scope.userDetails.selectedCity.city: ", $scope.userDetails.selectedCity.city);
				
				socket.emit('getAreas', {
					city: $scope.selectedCity
				}, function(data) {
					$scope.area = data;
					$scope.selectedArea = $scope.userDetails.selectedArea;
					console.log("socket getArea data: ", $scope.area);
				});
			});
		});*/

		socket.emit('getStates', null, function(data) {
			console.log('$scope.userDetails ', $scope.userDetails);
			$scope.states = data;
			$scope.selectedState = $scope.userDetails.selectedState;
			socket.emit('getCities', {
				state: $scope.selectedState
			}, function(data) {
				$scope.cities = data.data;
				$scope.selectedCity = $scope.userDetails.selectedCity;
				console.log('$scope.selectedCity ', $scope.selectedCity);
				socket.emit('getAreas', {
					city: $scope.selectedCity
				}, function(data) {
					$scope.area = data;
					$scope.selectedArea = $scope.userDetails.selectedArea;
					console.log('$scope.selectedArea: ', $scope.selectedArea);
				});
			});
		});

		$scope.getCity = function() {
			socket.emit('getCities', {
				state: $scope.selectedState
			}, function(data) {
				$scope.cities = data.data;
				console.log("socket getCities data: ", $scope.cities);
			});
		}


		$scope.getArea = function() {
			socket.emit('getAreas', {}, function(data) {
				$scope.area = data;
				console.log("socket getArea data: ", $scope.area);
			});
		}

		$scope.consultancyEdit = function() {
			$location.path('/consultancyEdit');
		};

		$scope.updateConsultancy = function(valid) {
			if (valid) {
				$scope.userDetails.selectedCity = $scope.selectedCity;
				$scope.userDetails.selectedArea = $scope.selectedArea;
				$scope.userDetails.selectedState = $scope.selectedState;
				socket.emit('updateConsultancy', $scope.userDetails, function(data) {
					$scope.consultancyData = data;
					$location.path('/consultancy');
				});
			} else {
				console.info('Some details are missing');
			}
		};
		
		$scope.sendJobMail = function(){
			$scope.userDetails = areaService.getUserLoginData();
			socket.emit('sendMail', {data:$scope.userDetails}, function(data) {
				console.log("Consultancy Send Mail ", data);
			});
		}
	}
]);

/*global jobGeeks*/
jobGeeks.controller('StudProfileViewCntlr', ['$scope', '$location', 'areaService', 'socket',  'upload', '$localStorage',
	function($scope, $location, areaService, socket, upload, $localStorage) {
		
		$scope.userDetails = areaService.getUserLoginData(); //get user details

		/*upload.listen("upload_input");*/ //add event listener to the file input
		
		upload.uploadOnClick("upload_button");//add event listener to the file input

    	socket.on('fileUploadSuccess', function (data) {//check file upload success status
        	$scope.completed =  data.success;
        	console.log('success: ', data.pathName);
        	
        	$scope.selectedImage = data.pathName.replace(/.*www/g,""); //trim absolute path to relative path
        	$scope.updateStudProfile();
    	});

		$scope.studEdit = function() {
			$location.path('/studEdit');
		};

		$scope.updateStudProfile = function() {
			/*$scope.userDetails.selectedCity = $scope.selectedCity;
			$scope.userDetails.selectedArea = $scope.selectedArea;
			$scope.userDetails.selectedState = $scope.selectedState;*/
			$scope.userDetails.selectedImage = $scope.selectedImage;
			console.log('#scope.userDetails', $scope.userDetails)
			socket.emit('updateConsultancy', $scope.userDetails, function(data) {
				console.log("socket updateStudProfile data: ", data);
				/*$location.path('/studentProfile');*/
			});
		};
	}
]);

/*global jobGeeks*/
jobGeeks.controller('StudProfileUpdateCntlr', ['$scope', '$location', 'areaService', 'socket',  'upload', '$localStorage',
	function($scope, $location, areaService, socket, upload, $localStorage) {
		
		$scope.userDetails = areaService.getUserLoginData(); //get user details

		upload.listen("upload_input"); //add event listener to the file input

    	socket.on('fileUploadSuccess', function (data) {//check file upload success status
        	$scope.completed =  data.success;
        	console.log('success: ', data.pathName);
        	
        	$scope.selectedImage = data.pathName.replace(/.*www/g,""); //trim absolute path to relative path
        	$scope.updateStudProfile();
    	});
    	
		socket.emit('getStates', null, function(data) { //prepopulate the user data and set the selected model
			console.log('$scope.userDetails ', $scope.userDetails);
			$scope.states = data;
			$scope.selectedState = $scope.userDetails.selectedState;
			socket.emit('getCities', {
				state: $scope.selectedState
			}, function(data) {
				$scope.cities = data.data;
				$scope.selectedCity = $scope.userDetails.selectedCity;
				console.log('$scope.selectedCity ', $scope.selectedCity);
				socket.emit('getAreas', {
					city: $scope.selectedCity
				}, function(data) {
					$scope.area = data;
					$scope.selectedArea = $scope.userDetails.selectedArea;
					console.log('$scope.selectedArea: ', $scope.selectedArea);
				});
			});
		});

		$scope.updateStudProfile = function() {
			$scope.userDetails.selectedCity = $scope.selectedCity;
			$scope.userDetails.selectedArea = $scope.selectedArea;
			$scope.userDetails.selectedState = $scope.selectedState;
			$scope.userDetails.selectedImage = $scope.selectedImage;
			console.log('#scope.userDetails', $scope.userDetails)
			socket.emit('updateConsultancy', $scope.userDetails, function(data) {
				console.log("socket updateStudProfile data: ", data);
				$location.path('/studentProfile');
			});
		};
	}
]);

/**
 * Created by Zuhaib on 2/11/2017.
 */
jobGeeks.controller('DashboardCtrl', [function () {
    console.log('Dashboard Loaded');
}]);

/**
 * Created by Zuhaib on 2/11/2017.
 */
jobGeeks.controller('FeedCtrl', [function () {
    console.log('Feed Loaded');
}]);