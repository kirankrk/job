/*global jobGeeks*/
jobGeeks.directive('errorBox', function() {
  return {
    restrict: 'E',
    transclude : false,
    templateUrl: './templates/error-component.html',
    scope: {
      errorMessage: '@',
      errorOption: '=',
      ngIf: '@',
      ngClass: '@'
    }
  };
});