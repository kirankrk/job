/*global jobGeeks*/
jobGeeks.controller('FilterCtrl', ['$scope', '$http', '$location', '$rootScope', 'areaService', 'socket', function($scope, $http, $location, $rootScope, areaService, socket) {
	$rootScope.urlData = 'https://job-kirankrk-2.c9users.io';
	$scope.userDetails = areaService.getUserLoginData();
	console.log('$scope.userDetails home : ', $scope.userDetails);
	/*	var empty = '', getStates = "/getStates";*/

	var reqObj = null;
	socket.emit('getStates', reqObj, function(data) {
		console.log('states from socket.io in socketServer : ', data);
		$scope.states = data;
	});

	/*var states = areaService.getData("", "/getStates");
	 */

	/*	states.success(function(response) {
			$scope.states = response;
			console.info("$scope.states from FilterCtrl: ", $scope.states)
		});*/

	/*$scope.getCity = function() {
		areaService.getData($scope.selectedState, "/getCities").success(function(response) {
			$scope.cities = response.data;
			console.log("$scope.cities $scope.selectedState in regController: ", $scope.cities, $scope.selectedState)
		});[]
	}*/
	$scope.getCity = function() {
		socket.emit('getCities', {
			state: $scope.selectedState
		}, function(data) {
			$scope.cities = data.data;
			console.log("socket getCities data: ", $scope.cities);
		});
	}


	$scope.getArea = function() {
			socket.emit('getAreas', {
				city: $scope.selectedCity.city
			}, function(data) {
				$scope.area = data.data;
				console.log("socket getArea data: ", $scope.area);
			});
		}
		/*$scope.getArea = function() {
			areaService.getData($scope.selectedCity, "/getAreas").success(function(response) {
				$scope.area = response;
				console.log("$scope.cities in regController: ", $scope.cities)
			});
		}*/

	$scope.getConsultancyList = function() {
		console.log("Get getConsultancyList: ", {
			selectedState: $scope.selectedState,
			selectedCity: $scope.selectedCity,
			selectedArea: $scope.selectedArea
		});

		/*var city = Json.stringfy($scope.selectedCity, function( key, value ) {
    			if( key === "$$hashKey" ) {
    			 return undefined;
    			}
    		return value;
		});*/

		//	
		//console.log('json city list', city);

		var obj = {
			selectedState: $scope.selectedState,
			selectedCity: $scope.selectedCity,
			selectedArea: $scope.selectedArea
		};
		delete obj.selectedCity.$$hashKey;
		console.log("obj " + obj);
		socket.emit('getConsultancyList', obj, function(data) {
			$scope.consultancies = data.data;
			console.info('socket consultancy list', data);
		});
	};

	$scope.getStudentProfile = function() {
		console.info('my profile');
		$location.path('/studentProfile');
	}

	$scope.getConsultancyData = function(selectedConsultancy) {
		socket.emit('getConsultancyData', {
			email: selectedConsultancy
		}, function(response) {
			console.log('socket consultancy list', response);
			$scope.userDetails.userInfo=response.data[0];
			$scope.userDetails.userInfo.userType = 'student';
			areaService.setConsultancyData(response.data[0]);
			$location.path('/consultancy');
		});

	};
}]);
