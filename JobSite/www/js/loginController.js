/*global jobGeeks*/
jobGeeks.controller('loginCtrl',['$scope', 'areaService', '$localStorage', '$location', 'socket' ,
function($scope, areaService, $localStorage, $location, socket) {
	$scope.user = {};
	$scope.validateEmail = areaService.validateEmail; //email validation
	$scope.user = areaService;
	$scope.obj = {value: 'error'};//css
	$scope.errorMessage;//show errormessage
	$scope.showError = false; //show error
	$scope.login = function() {
		var url = "/checkLogin";
		
		areaService.checkLogin($scope.user, url).success(function(response) {
		//	var _dataArray = areaService._dataArray;
			console.log('response: ', response);
			if(response.SUCCESS){
				areaService.setUserDetails(response);
					areaService.isLogin(response);	
			}
			else{
				$scope.showError = true;
				$scope.errorMessage =response.message;//show errormessage
			}
		});
	};


	$scope.logOut = function() {
		delete $localStorage.userDetails;
		areaService.userDetails = undefined;
		$location.path('/login');
		/*areaService.loginData = {logOutIcon: false, homeIcon: false, registerIcon: false,  loginIcon: true};
		$scope.loginData = areaService.loginData;*/
		console.info('Logged out');
	};
}]);
