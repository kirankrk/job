/*global jobGeeks, SocketIOFileUpload*/
jobGeeks.controller('regCtrl', function($scope, $http, $location, $rootScope, areaService, socket) {

    $scope.user = {}; //user data model
    $scope.showError;
    $scope.errorMessage;
	var states = areaService.getData("", "/getStates");
	
	$scope.showForm = {
		name: 'student'
	}

	
	$scope.validateEmail = areaService.validateEmail; //email validation
	$scope.validatePassword = areaService.validatePassword;
	$scope.validateContact= areaService.validateContact;

	$scope.isShown = function(whichForm) {
		$scope.showForm.name = whichForm;
		console.log('$scope.showForm.name: ', $scope.showForm.name);
		/*return whichForm === $scope.user.userType;*/
	};

	states.success(function(response) {
		$scope.states = response;
		console.log("$scope.states: ", $scope.states)
	});

	$scope.getCity = function() {
		$scope.cities = '';
		areaService.getData($scope.user.selectedState, "/getCities").success(function(response) {
			$scope.cities = response.data;
			console.log("$scope.cities: ", $scope.cities)
		});
	}

	$scope.getArea = function() {
		areaService.getData($scope.user.selectedCity, "/getAreas").success(function(response) {
			$scope.area = response;
			console.log("$scope.area: ", $scope.area)
		});
	}

	$scope.register = function() {
		console.log('areaService: ', areaService)
		console.info('$scope.user: ', $scope.user);
		console.info("$scope.showForm.name ", $scope.showForm.name);
		console.info("$scope.user.email ", $scope.user.email);

		$http({
				url : '/registerUser',
				method : 'POST',
				data : $scope.user
			}).success(function(response) {
			
				if(response.SUCCESS){
					areaService.setUserDetails(response);
					console.info("response.SUCCESS ", response.message);
					console.info("response.SUCCESS in if", response.message);
					$location.path('/login');
				}
				else{
					$scope.showError = true;
					$scope.errorMessage="Please check the filled information "+ response.message;
					console.info("response.SUCCESS in else", response.message);
				     $location.path('/register');	
				}
				
			});

	};
});