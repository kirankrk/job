/*global jobGeeks*/
jobGeeks.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'templates/Login.html',
		controller : 'loginCtrl',
		requireLogin: false
	}).when('/login', {
		templateUrl : 'templates/Login.html',
		controller : 'loginCtrl',
		requireLogin: false
	}).when('/home', {
		templateUrl : 'templates/Home.html',
		controller : 'FilterCtrl',
		requireLogin: true
	}).when('/register', {
		templateUrl : 'templates/Register.html',
		controller : 'regCtrl',
		requireLogin: false
	}).when('/studentProfile', {
		templateUrl : 'templates/StudentProfileView.html',
		controller : 'StudProfileViewCntlr',
		requireLogin: true
	}).when('/consultancy', {
		templateUrl : 'templates/ConsultancyDetails.html',
		controller : 'consultancyCtrl',
		requireLogin: true
	}).when('/consultancyEdit', {
		templateUrl : 'templates/ConsultancyEdit.html',
		controller : 'consultancyCtrl',
		requireLogin: true
	}).when('/studEdit', {
		templateUrl : 'templates/StudProfileUpdate.html',
		controller : 'StudProfileUpdateCntlr',
		requireLogin: true
	}).when('/dashboard', {
		templateUrl: 'templates/dashboard.html',
		controller: 'DashboardCtrl',
		requireLogin: true
	}).otherwise({
		redirectTo : '/login'
	});
} 
]);


