jobGeeks.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'templates/Home.html',
		controller : 'FilterCtrl'
	}).when('/login', {
		templateUrl : 'templates/Login.html',
		controller : 'loginCtrl'
	}).when('/register', {
		templateUrl : 'templates/Register.html',
		controller : 'regCtrl'
	}).when('/consultancy', {
		templateUrl : 'templates/ConsultancyDetails.html',
		controller : 'consultancyCtrl'
	}).when('/consultancyEdit', {
		templateUrl : 'templates/ConsultancyEdit.html',
		controller : 'consultancyCtrl'
	}).otherwise({
		redirectTo : '/login'
	});
} ]);


