/*global jobGeeks, io*/
/**
 * Created by Zuhaib Kathwari on 20-06-2016.
 */
jobGeeks.service('areaService', ['$http', '$localStorage', '$rootScope', '$route', '$log', '$location', function($http, $localStorage, $rootScope, $route, $log, $location) {

	this._dataArray = [];
	this._isLogin = false;
	this.validateEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	this.validatePassword = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/i;
	this.validateContact = /^(\+\d{1,3}[- ]?)?\d{10}$/;
		
		this.userName= function(){//this subroutine checks who is logged in
			if(this.userDetails != undefined) {
				if( this.userDetails.userInfo.userType != 'student'){
					this.name = this.userDetails.userInfo.name;
				}
				else{
				 this.name = this.userDetails.userInfo.fName + " " + this.userDetails.userInfo.lName;
				}
					return {
						name: this.name,//incase of consultancy use this conditionally
						userType : this.userDetails.userInfo.userType,
						isLogin: true
					};
			} else {
				throw "User not logged in!"
			}
		}
	
	

	this.getData = function(states, addressData) {
		return $http({
			url: addressData,
			method: 'POST',
			data: {
				state: states
			}
		});
	};

	this.getConsultancyDataItems = function(selected, addressData) {
		return $http({
			url: addressData,
			method: 'POST',
			data: {
				email: selected
			}
		});
	};

	this.setConsultancyData = function(consultancyData) {
		this.consultancyData = consultancyData;
	};

	this.getConsultancyData = function() {
		return this.consultancyData;
	};


	this.setUserDetails = function(userDetails) {
		this.userDetails = userDetails;
	};

	this.getUserDetails = function() {
		return this.userDetails;
	};
	
	this.checkLogin = function(data, url) {
		return $http({
			url: url,
			method: 'POST',
			data: data
		});
	};

	this.loginDataStore = function() { //local storage
		$localStorage.userDetails = {
			userDetails: this.userDetails,
			'isLoggedIn' : true
		};
		console.info('Data Stored in local storage', $localStorage.userDetails);
	};
	
	this.getUserLoginData = function (){
		if(this.userDetails === undefined && $localStorage.userDetails.userDetails !== undefined) {
			this.userDetails = $localStorage.userDetails.userDetails;
			console.info('$localStorage.userDetails: ', $localStorage.userDetails.userDetails)
			return this.userDetails.userInfo;
		} else {
			console.info('$localStorage.userDetails: ', $localStorage.userDetails.userDetails)
			return this.userDetails.userInfo;
		}
	}

	this.isLogin = function(userLoginData) {
		console.info('user about to login: ', userLoginData);
		if (userLoginData.SUCCESS) {
			this.userDetails = userLoginData.data;
			this.loginDataStore();
			this._isLogin = true;
			/*this.userDetails = userLoginData.data;*/
			if (userLoginData.userType == 'student') {
				console.warn("Student Logged in");
				//this._dataArray = userLoginData.data;
				$location.path('/home');
			}
			else if (userLoginData.userType == 'consultancy') {
				//this.setConsultancyData(userLoginData.data.userInfo);
				console.log('areaService.consultancyData: ', this.consultancyData);
				$location.path('/consultancy');
			}
			else {
				this._isLogin = false;
			}
		}
		else {
			console.warn("WRONG_USERNAME_PASSWORD");
		}
	};
}]);




//Socket.io API
jobGeeks.factory('socket', function($rootScope) {
	var socket = io.connect();
	return {
		socketInstance: socket,
		on: function(eventName, callback) {
			socket.on(eventName, function() {
				var args = arguments;
				$rootScope.$apply(function() {
					callback.apply(socket, args);
				});
			});
		},
		emit: function(eventName, data, callback) {
			socket.emit(eventName, data, function() {
				var args = arguments;
				$rootScope.$apply(function() {
					if (callback) {
						callback.apply(socket, args);
					}
				});
			});
		}
	};
});