/*global  jobGeeks, SocketIOFileUpload*/
/**
 * Created by Zuhaib Kathwari on 12/7/2016.
 */
jobGeeks.factory('upload', ['socket', '$rootScope', function(socket, $rootScope) {
    console.log('upload factory loaded yes');
    // Initialize instances:
    var socket = socket.socketInstance;
    
    var siofu = new SocketIOFileUpload(socket);

    return {
        listen: function (input) {
            siofu.listenOnInput(document.getElementById(input));
        },
        uploadOnClick: function (input) {
            //takes element id as input e.g button id
            document.getElementById(input).addEventListener("click", siofu.prompt, false);
        }
    }
}]);