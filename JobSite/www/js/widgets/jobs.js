/**
 * Created by Zuhaib on 2/22/2017.
 */
jobGeeks.controller('JobsCtrl', ['$scope', function ($scope) {
    console.log('JobsCtrl Loaded');
    $scope.createJobModel = [
        {"caption": "Job Name", "model": ""},
        {"caption": "Details", "model": ""},
        {"caption": "Qualification", "model": ""},
        {"caption": "Keywords", "model": ""},
        {"caption": "Experience", "model": ""},
        {"caption": "Criteria", "model": ""},
        {"caption": "CTC", "model": ""},
        {"caption": "Company Name", "model": ""}
    ];

    $scope.createJob = function () {
        console.log('Form Submitted: ', $scope.createJobModel);
    }
}]);
